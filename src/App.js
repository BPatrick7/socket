import React, { Component } from "react";
import io from "socket.io-client";

class App extends Component {
  state = {
    isConnected: false,
    id: null,
    peeps: [],
    answer: "",
    old_messages: [],
    new_messages: []
  };
  socket = null;

  componentDidMount() {
    this.socket = io("http://codicoda.com:8000");

    this.socket.on("connect", () => {
      this.setState({ isConnected: true });
    });

    {
      /* this.socket.on("youare", answer => {
      this.setState({ id: answer.id });
    }); */
    }

    this.socket.emit("whoami");
    this.socket.on("youare", answer => {
      this.setState({ id: answer.id });
    });

    this.socket.on("peeps", answer => {
      this.setState({ peeps: answer });
    });

    this.socket.on("pong!", additionalStuff => {
      console.log("the server answered!", additionalStuff);
    });

    this.socket.on("next", message_from_server =>
      console.log(message_from_server)
    );

    this.socket.on("disconnect", () => {
      this.setState({ isConnected: false });
    });

    /** this will be useful way, way later **/
    this.socket.on("room", old_messages => console.log(old_messages));
  }

  componentWillUnmount() {
    this.socket.close();
    this.socket = null;
  }

  render() {
    const info = { text: "text", id: this.state.id, name: "Bassell" };

    {
      /* Fifth Msg
    Congrats! You can send and receive chat messages. Now: 
     1. display the old messages (you're receiving them when listening to `room`). Just take this array and put it in state and render it.
     2. display the new message (add it to the array) 
     3. make the id auto-loaded, so you don't have to click on the button evey time to obtain an id. 
    4. make it look the best you can! */
    }
    return (
      <div className="App">
        <div>
          status: {this.state.isConnected ? "connected" : "disconnected"}
        </div>
        <div>id: {this.state.id}</div>
        <button onClick={() => this.socket.emit("ping!")}>ping</button>
        <button onClick={() => this.socket.emit("whoami")}>Who am I?</button>

        {/*<button onClick={() => this.socket.emit("give me next")}>
          Give me next!
        </button> */}

        {/* First Msg Bravo! Now please emit "give me next" from the client. You
        can do that on start, or when pressing a button, whatever App.js:28 the
        server answered! */}

        {/* Second Msg
        Impressive. We're going to top it up a notch. 
        The server will send you a simple, random, addition.You will need to answer correctly.
        You cannot put the answer in a button, because if you do, the page will refresh, and you will lose your ID. 
        Try to solve it! The API to get an equation is to send "addition" */}

        {/* <button onClick={() => this.socket.emit("addition")}>addition</button> */}

        <button onClick={() => this.socket.emit("message", info)}>
          Send a message
        </button>

        {/* Third Msg
        please send the message "answer" with the answer to 4 + 87 + 7. 
        You may send the answer as a string. You may also get a hint.
         */}

        <div>
          <h3> Peeps List Connected: {this.state.id} </h3>
          <ul>
            <p> {this.state.peeps.length} Online Now </p>
            {this.state.peeps
              ? this.state.peeps.map(x => <li key={x}> {x} </li>)
              : "null"}
          </ul>
        </div>

        <div>
          {/* Fourth Msg
        Nice! You solved it. You can now send arbitrary things to the server. That's cool. 
        Let's take a moment to clean up. We're going to keep: 
        1. the "whoami" button 
        2. the input that sends text 
        3. the button that sends that text, You can also keep the labels that say if you are connected or not.
        Remove everything else. What you'll also do is, instead of "answer", make the button submit to an endpoint called "message". 
        You can send any piece of text there. See what happens. Do clean first though! */}

          <input
            type="text"
            name="answer"
            onChange={e => this.setState({ answer: e.target.value })}
          />
          <button onClick={() => this.socket.emit("answer", this.state.answer)}>
            submit answer
          </button>
        </div>
      </div>
    );
  }
}

export default App;
